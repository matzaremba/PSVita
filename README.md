# PSVita

Run:

`git clone https://github.com/matt360/PSVita.git`

`git submodule init`

`git submodule update`

Visual Studio 2015:

go to: `Debug/scene_app Properties...`

choose: `Configuration: All Configurations`

then in the `scene_app Property Pages` go to: `Configuration Properties/Debugging`

and in the `Working Directory` put: `$(ProjectDir)../../media`
